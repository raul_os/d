﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamAppSageV1.Paginas
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MenuPrincipal : ContentPage
	{
		public MenuPrincipal ()
		{
			InitializeComponent ();
		}

        protected override void OnAppearing()
        {
            base.OnAppearing();
            
        }

        public void btnconfigurar_Click(object sender, EventArgs a) { }

        public void ConsultaArticulos_Click(object sender, EventArgs a)
        {
            ConsultaArticuloCodigo pagina1 = new ConsultaArticuloCodigo();
            Navigation.PushAsync(pagina1);
        }

        public void ListaArticulos_Click(object sender, EventArgs a)
        {
            PaginaListaArticulos pagina = new PaginaListaArticulos();
            Navigation.PushAsync(pagina);
        }
    }
}
