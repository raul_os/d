﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamAppSageV1.Clases;

namespace XamAppSageV1.Paginas
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PaginaListaArticulos : ContentPage
	{
		public PaginaListaArticulos ()
		{
			InitializeComponent ();
		}
        protected override void OnAppearing()
        {
            base.OnAppearing();
            lsvArticulos.ItemsSource = BaseDeDatos.ObtenerArticulos();
        }

        private void lsvArticulos_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem != null)
                NavegarArticulo(e.SelectedItem as Articulos);
        }

        void btnNuevo_Click(object sender, EventArgs a)
        {
            NavegarArticulo(new Articulos());
        }

        void NavegarArticulo(Articulos articulo)
        {
            PaginaArticulo pagina = new PaginaArticulo();
            pagina.Articulo = articulo;
            Navigation.PushAsync(pagina);
        }
    }
}
