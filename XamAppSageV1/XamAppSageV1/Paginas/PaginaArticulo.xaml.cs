﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamAppSageV1.Clases;

namespace XamAppSageV1.Paginas
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PaginaArticulo : ContentPage
	{
        public Articulos Articulo;
        public PaginaArticulo ()
		{
			InitializeComponent ();
		}


            protected override void OnAppearing()
            {
                base.OnAppearing();

                BindingContext = this.Articulo;
            }

            void btnGuardar_Click(object sender, EventArgs a)
            {
                if (Articulo.CodigoArticulo.Length == 0)
                    BaseDeDatos.AgregarArticulo(Articulo);
                else
                    BaseDeDatos.ModificarArticulo(Articulo);

                Navigation.PopAsync();
            }

            void btnEliminar_Click(object sender, EventArgs a)
            {
                if (Articulo.CodigoArticulo.Length != 0)
                {
                    BaseDeDatos.EliminarArticulo(Articulo);
                    Navigation.PopAsync();
                }
            }
       
    }
}
