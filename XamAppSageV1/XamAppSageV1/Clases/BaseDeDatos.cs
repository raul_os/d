﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace XamAppSageV1.Clases
{
    public class BaseDeDatos
    {
        static string cadenaConexion = @"data source=asus-t-ra;initial catalog=Sage;user id=logic;password=Sage2009+;Connect Timeout=60";

        public static List<Articulos> ObtenerArticulos()
        {
            List<Articulos> listaArticulos = new List<Articulos>();
            string sql = "SELECT * FROM Articulos";

            using (SqlConnection con = new SqlConnection(cadenaConexion))
            {
                con.Open();

                using (SqlCommand comando = new SqlCommand(sql, con))
                {
                    using (SqlDataReader reader = comando.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            try
                            {
                                Articulos articulo = new Articulos()
                                {
                                    CodigoEmpresa = reader.GetInt16(0),
                                    CodigoArticulo = reader.GetString(1),
                                    DescripcionArticulo = reader.GetString(2),
                                    CodigoAlternativo = reader.GetString(3),
                                 //   PrecioVenta = reader.GetDecimal(4)
                                };
                                listaArticulos.Add(articulo);
                            }
                            catch { }
                        }
                    }
                    con.Close();

                    return listaArticulos;
                }
            }
        }

        //Agrega Articulos
        public static void AgregarArticulo(Articulos articulo)
        {
            string sql = "INSERT INTO Articulos (CodigoEmpresa, CodigoArticulo, DescripcionArticulo,) VALUES(@codigoempresa, @codigoarticulo, @descripcionarticulo)";

            using (SqlConnection con = new SqlConnection(cadenaConexion))
            {
                con.Open();

                using (SqlCommand comando = new SqlCommand(sql, con))
                {
                    comando.Parameters.Add("@codigoempresa", SqlDbType.Int, 100).Value = articulo.CodigoEmpresa;
                    comando.Parameters.Add("@codigoarticulo", SqlDbType.VarChar, 20).Value = articulo.CodigoArticulo;
                    comando.Parameters.Add("@descripcionarticulo", SqlDbType.VarChar, 40).Value = articulo.DescripcionArticulo;
                    comando.CommandType = CommandType.Text;
                    comando.ExecuteNonQuery();
                }

                con.Close();
            }
        }

        //Modifica Articulo
        public static void ModificarArticulo(Articulos articulo)
        {
            string sql = "UPDATE Articulos set DescripcionArticulo = @descripcionarticulo, PrecioVenta = @precioventa WHERE CodigoEmpresa = @codigoempresa AND CodigoArticulo = @codigoarticulo";

            try
            {
                using (SqlConnection con = new SqlConnection(cadenaConexion))
                {
                    con.Open();

                    using (SqlCommand comando = new SqlCommand(sql, con))
                    {
                        comando.Parameters.Add("@descripcionarticulo", SqlDbType.VarChar, 40).Value = articulo.DescripcionArticulo;
                        comando.Parameters.Add("@precioventa", SqlDbType.Decimal).Value = articulo.PrecioVenta;
                        comando.Parameters.Add("@codigoempresa", SqlDbType.Int).Value = articulo.CodigoEmpresa;
                        comando.Parameters.Add("@codigoarticulo", SqlDbType.VarChar, 20).Value = articulo.CodigoArticulo;
                        comando.CommandType = CommandType.Text;
                        comando.ExecuteNonQuery();
                    }

                    con.Close();
                }
            }
            catch (Exception ex)
            {

            }
        }

        //Eliminar Artículo
        public static void EliminarArticulo(Articulos articulo)
        {
            string sql = "DELETE FROM Articulos WHERE CodigoArticulo = @codigoarticulo AND CodigoEmpresa = @codigoempresa";

            using (SqlConnection con = new SqlConnection(cadenaConexion))
            {
                con.Open();

                using (SqlCommand comando = new SqlCommand(sql, con))
                {
                    comando.Parameters.Add("@codigoarticulo", SqlDbType.VarChar, 20).Value = articulo.CodigoArticulo;
                    comando.Parameters.Add("@codigoempresa", SqlDbType.Int).Value = articulo.CodigoEmpresa;
                    comando.CommandType = CommandType.Text;
                    comando.ExecuteNonQuery();
                }

                con.Close();
            }
        }
    }
}
