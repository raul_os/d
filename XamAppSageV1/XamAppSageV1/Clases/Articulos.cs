﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Data;

namespace XamAppSageV1.Clases
{
    public class Articulos
    {
        public int CodigoEmpresa { get; set; }
        public string CodigoArticulo { get; set; }
        public string DescripcionArticulo { get; set; }
        public string CodigoAlternativo { get; set; }
        public decimal PrecioVenta { get; set; }
        public decimal PrecioCompra { get; set; }
        public string CodigoFamilia { get; set; }
        public string CodigoSubFamilia { get; set; }
        public string CodigoProveedor { get; set; }
    }
}
